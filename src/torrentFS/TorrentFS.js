import WebTorrent from './webtorrent.min.js';
const client = new WebTorrent();
document.querySelector('#statWebtorrent').innerHTML = '<span style="color: green">Ok</span>'

export class TorrentFS {
	constructor(magnetURI) {
		this.magnetURI = magnetURI
	}
	
	open() {
		return new Promise((resolve, reject) => {
			try {
				client.add(this.magnetURI, (torrent) => {
					torrent.deselect(0, torrent.pieces.length - 1)
					this._torrent = torrent

					document.querySelector('#statTorrent').innerHTML = '<span style="color: green">Ok</span>'
					document.querySelector('#search').disabled = false;
					document.querySelector('#button').disabled = false;

					displayStats(torrent);

					resolve()
				})
			} catch (err) {
				reject(err)
			}
		})
	}

	read(from, nBytes) {
		const fromPiece = Math.floor(from / this._torrent.pieceLength)
		const nPieces = Math.ceil(nBytes / this._torrent.pieceLength)

		this._torrent.critical(fromPiece, fromPiece + nPieces - 1)
		
		return new Promise((resolve, reject) => {
			try {
				const request = this._torrent.files[0].createReadStream({ start: from, end: from + nBytes - 1})
				request.on('data', (chunk) => {
					resolve(chunk)
				})
			} catch (err) {
				reject(err)
			}
		})
	}

	size() {
		return this._torrent.length
	}
}

function speedString(speedInBytes) {
	const unit = ['B', 'KiB', 'MiB', 'GiB', 'TiB']
	let speed = parseFloat(speedInBytes)
	let i = 0
	for (; speed > 1024; i++) {
		speed /= 1024
	}
	return speed.toFixed(2) + ' ' + unit[i]
}

function displayStats(torrent) {
	document.querySelector('#statPeers').innerHTML = '<span style="color: green">' + torrent.numPeers + '</span>'
	document.querySelector('#statSpeed').innerHTML = '<span>' + speedString(torrent.downloadSpeed) + '/s</span>'
	document.querySelector('#statDownloaded').innerHTML = '<span>' + speedString(torrent.downloaded) + '</span>'

	setInterval(() => {
		document.querySelector('#statPeers').innerHTML = '<span style="color: green">' + torrent.numPeers + '</span>'
		document.querySelector('#statSpeed').innerHTML = '<span>' + speedString(torrent.downloadSpeed) + '/s</span>'
		document.querySelector('#statDownloaded').innerHTML = '<span>' + speedString(torrent.downloaded) + '</span>'
	}, 2500)
}
