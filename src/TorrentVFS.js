// Copyright 2021 Roy T. Hashimoto. All Rights Reserved.
import * as VFS from './VFS.js';
import { TorrentFS } from './torrentFS/TorrentFS.js';

// Asynchronous memory filesystem. This filesystem requires an Asyncify
// build. It is mainly useful for testing that the Asyncify build is
// working.
export class TorrentVFS extends VFS.Base {
	name = 'torrent';
	
	constructor(magnetURI) {
		super();
		this._torrentFS = new TorrentFS(magnetURI);
	}

	/**
	 * @param {string?} name 
	 * @param {number} fileId 
	 * @param {number} flags 
	 * @param {{ set: function(number): void }} pOutFlags 
	 * @returns {number|Promise<number>}
	 */
	xOpen(name, fileId, flags, pOutFlags) {
		return this.handleAsync(async () => {
			await this._torrentFS.open()
			pOutFlags.set(flags);
			return VFS.SQLITE_OK;
		});
	}

	/**
	 * @param {number} fileId 
	 * @returns {number|Promise<number>}
	 */
	xClose(fileId) {
		return this.handleAsync(async () => {
			return VFS.SQLITE_OK;
		});
	}

	/**
	 * @param {number} fileId 
	 * @param {{ size: number, value: Int8Array }} pData 
	 * @param {number} iOffset
	 * @returns {number|Promise<number>}
	 */
	xRead(fileId, pData, iOffset) {
		return this.handleAsync(async () => {
			const data = await this._torrentFS.read(iOffset, pData.size)

			if (data.length) {
				pData.value.set(new Int8Array(data, 0, data.length));
			}

			if (data.length < pData.size) {
				pData.value.fill(0, data.length);
				return VFS.SQLITE_IOERR_SHORT_READ;
			}
			return VFS.SQLITE_OK
		});
	}

	/**
	 * @param {number} fileId 
	 * @param {{ size: number, value: Int8Array }} pData 
	 * @param {number} iOffset
	 * @returns {number|Promise<number>}
	 */
	xWrite(fileId, pData, iOffset) {
		return this.handleAsync(async () => {
			return VFS.SQLITE_OK;
		});
	}

	/**
	 * @param {number} fileId 
	 * @param {number} iSize 
	 * @returns {number|Promise<number>}
	 */
	xTruncate(fileId, iSize) {
		return this.handleAsync(async () => {
			return VFS.SQLITE_OK;
		});
	}

	/**
	 * @param {number} fileId 
	 * @param {{ set: function(number): void }} pSize64 
	 * @returns {number|Promise<number>}
	 */
	xFileSize(fileId, pSize64) {
		return this.handleAsync(async () => {
			pSize64.set(this._torrentFS.size())
		});
	}

	/**
	 * 
	 * @param {string} name 
	 * @param {number} syncDir 
	 * @returns {number|Promise<number>}
	 */
	xDelete(name, syncDir) {
		return this.handleAsync(async () => {
			return VFS.SQLITE_OK;
		});
	}

	/**
	 * @param {string} name 
	 * @param {number} flags 
	 * @param {{ set: function(number): void }} pResOut 
	 * @returns {number|Promise<number>}
	 */
	xAccess(name, flags, pResOut) {
		return this.handleAsync(async () => {
			pResOut.set(0);
		return VFS.SQLITE_OK;
		});
	}
}
