# p2pSearch

p2pSearch is a static torrent search engine that does peer-to-peer queries over bittorrent, by asking for pieces of a database seeded by peers as a torrent to complete them.

## License disclaimer

I just copied some files and changed few lines of code of:
- wa-sqlite: [https://github.com/rhashimoto/wa-sqlite](https://github.com/rhashimoto/wa-sqlite) (GPL 3.0)
	- MemoryAsyncVFS.js
	- MemoryVFS.js
	- sqlite-api.js
	- sqlite-constants.js
	- VFS.js
	- wa-sqlite-async.mjs
- WebTorrent: [https://github.com/webtorrent/webtorrent](https://github.com/webtorrent/webtorrent) (MIT)
	- webtorrent.min.js

so please stick to their respective licenses.

## Installation

```text
$ yarn install
$ ./node_modules/.bin/webpack
$ python -m http.server 8080
```

## Reproduce

Steps required to reproduce this repo are explained in [this post](https://boredcaveman.xyz/post/0x2_static-torrent-website-p2p-queries.html)
