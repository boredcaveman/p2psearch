import SQLiteAsyncESMFactory from './src/wa-sqlite-async.mjs'
import * as SQLite from './src/sqlite-api.js'
import { TorrentVFS } from './src/TorrentVFS.js'

const magnetURI = 'magnet:?xt=urn:btih:PASTE+YOUR+MAGNET+HERE!'

let sqlite3a = {}
let db = {}
const initializeDb = (async function() {
	const SQLiteAsyncModule = await SQLiteAsyncESMFactory()
	sqlite3a = SQLite.Factory(SQLiteAsyncModule)
	sqlite3a.vfs_register(new TorrentVFS(magnetURI))
	db = await sqlite3a.open_v2('torrent', undefined, 'torrent')
})()

async function search(input) {
	await initializeDb
	tableBody.innerHTML = ''

	const query = `SELECT * FROM torrents WHERE title MATCH '` + input + `' LIMIT 20;`
	const callback = addRowToTable
	document.querySelector('#search').disabled = true;
	document.querySelector('#button').disabled = true;
	exec(db, query, callback)
}

let newQuery = false
let execFinished = new Promise((resolve, reject) => resolve())
async function exec(db, query, callback) {
	newQuery = true
	await execFinished
	newQuery = false

	let firstResult = true;

	execFinished = new Promise((resolve, reject) => async function() {
		for await (const stmt of sqlite3a.statements(db, query)) {
			const columns = callback ? sqlite3a.column_names(stmt) : null;
			while (await sqlite3a.step(stmt) === SQLite.SQLITE_ROW) {
				if (firstResult) {
					document.querySelector('#search').disabled = false;
					document.querySelector('#button').disabled = false;
					firstResult = false;
				}
				if (newQuery) {
					break;
				}
				if (callback) {
					const row = sqlite3a.row(stmt);
					callback(row, columns);
				}
			}
			if (newQuery) {
				break;
			}
		}
		document.querySelector('#search').disabled = false;
		document.querySelector('#button').disabled = false;
		resolve(SQLite.SQLITE_OK)
	}())
}

/* DOM */

const searchBox = document.querySelector('#search')
const button = document.querySelector('#button')
const tableBody = document.querySelector('#tbody')

// const results = document.querySelector('#results')

button.addEventListener('click', () => {
	const input = searchBox.value.trim()
	search(input)
})

searchBox.addEventListener('keypress', (e) => {
	var code = (e.keyCode ? e.keyCode : e.which);
	if (code == 13) {
		const input = searchBox.value.trim()
		search(input)
	}
})


function addRowToTable(row, column) {
	let rowHTML = '<tr>'
	rowHTML += '<td>' + row[1] + '</td>'
	rowHTML += '<td>' + speedString(parseInt(row[2])) + '</td>'
	rowHTML += '<td><a href="magnet:?xt=urn:btih:' + row[0] + '">Link</a></td></tr>'
	tableBody.innerHTML += rowHTML
}

function speedString(speedInBytes) {
	const unit = ['B', 'KiB', 'MiB', 'GiB', 'TiB']
	let speed = parseFloat(speedInBytes)
	let i = 0
	for (; speed > 1024; i++) {
		speed /= 1024
	}
	return speed.toFixed(2) + ' ' + unit[i]
}
